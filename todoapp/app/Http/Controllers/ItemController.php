<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Http\Resources\ItemResource;
use App\Http\Resources\ItemResourceCollection;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): ItemResourceCollection
    {
        return new ItemResourceCollection(Item::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Items = new Item();

        $Items->name = $request->input('name');
        $Items->inStock = $request->input('inStock');
        $Items->price = $request->input('price');
        $Items->description = $request->input('description');
        $Items->status = $request->input('status');
        // $validated = $request->validate([
        //     'name' => 'required',
        //     'inStock' => 'required',
        //     'price' => 'required',
        //     'description' => 'required',
        //      'status' => 'required',
        // ]);
        $Items->save();
        return response()->json($Items);
        // $Item = Item::create($validated);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Item $Item): ItemResource
    {
        return new ItemResource ($Item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $Item):ItemResource
    {
        $Item->update($request->all());

        return new ItemResource($Item);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $Item)
    {
        $Item->delete();

        return response()->json(null, 204);
    }
}
