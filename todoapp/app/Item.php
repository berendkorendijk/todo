<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    
    protected $fillable =[
        'name',
        'inStock',
        'price',
        'description',
         'status',
    ];
}
