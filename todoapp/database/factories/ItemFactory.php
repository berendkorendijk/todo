<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'inStock'=> random_int(0,1000),
        'price'=> random_int(0, 100),
        'description'=>$faker->text,
    ];
});
