import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import TodoInput from './components/todoInput';
import TodoItem from './components/todoItem';
import Modal from './components/Modal/Modal';

class App extends Component {
  showModal = () =>{
    this.setState({
     ... this.state,
      show: !this.state.show
    });
  }
  state = {
    nextID: 0,
    Items: [

    ]
  }
  componentDidMount() {
    fetch('http://127.0.0.1:8000/api/items')
    .then(res => res.json())
    .then((data) => {
      this.setState({ Items: data.data })
      console.log(data);
    })
    .catch(console.log)
  }
  
  constructor(props){
    super(props);

    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    // nextID: Items.id;
  }

  addTodo(todoText){
    this.state.Items.push({id: this.state.nextID, description: todoText});
    this.setState({
      nextID: ++this.state.nextID
    });
  }

  removeTodo(id){
    // this.setState({
    //   data: this.state.data.filter((todo, id) => todo.id !== id)
    // })

    console.log(id);
    fetch('http://127.0.0.1:8000/api/items/' + id, {
      method: 'DELETE',
    })
    .then( res => {
      console.log("success: " + res);
      window.location.reload();
    })
    .catch(err => {
      console.log("NOT WORKING RADESH: " + err)
    })
  }
  render(){
    const {Items} = this.state;
  return (
    
    <div className="App">
     <div className="todo-wrapper">
       <Header></Header>
       <input type="button"
       onClick={this.showModal}
        value="Add Todo" />
       <Modal 
       onClose={this.showModal}
       show={this.state.show}>
          Create a todo
       </Modal>
       <TodoInput></TodoInput>
       <ul>
          {Items.map(todo => {
             
             return <TodoItem todo ={todo} key={todo.id} id ={todo.id} removeTodo={this.removeTodo} />
            
           })
          }
         </ul> 
      </div>
     </div>
     );
    }
}
export default App;
