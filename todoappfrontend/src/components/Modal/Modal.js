import React from 'react';
import PropTypes from 'prop-types';
import TodoInput from '../todoInput';
import todoItem from '../todoItem';
import componentDidMount from '../../App'
const backDropStyle ={
    position: 'fixed',
    top:0,
    bottom:0,
    left:0,
    right:0,
    backgroundClose: 'rgba(0,0,0,0.3)',
    padding:50
}

const modalStyle ={
    backgroundColor: 'blue',
    borderRadius:5,
    maxWidth: 490,
    minHeight:250,
    margin: '0 auto',
    padding: 30,
    position: 'relative'
};

const footerStyle={
    position: "absolute",
    bottom:20
}
const textField={
    width: 400,
    top: 40,

}


export default class Modal extends React.Component{

    
    constructor(props){
        super(props);
        this.state ={value: " "};
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
      }


    onClose =(e) =>{
       console.log("BUTTON CLICKED");
       e.stopPropagation ();
        this.props.onClose && this.props.onClose(e);
    }

    onKeyUp = (e) =>{
        //Toets 27 is escape
        if(e.which === 27 && this.props.show){
            this.onClose(e);
        }
    }

    componentDidMount(){
        document.addEventListener("keyup", this.onKeyUp);
    }

    componentWillMount(){
        document.removeEventListener("keyup", this.onKeyUp); 
    }

      handleChange(event, error){
        this.setState({value: event.target.value});

        if(this.state.value === undefined){
        console.log(error);
        }
        else{
            console.log(this.state.value);
        }

    }
    handleSubmit(event){
        event.preventDefault();
       let inputField = event.target;

       let data = {
           name : inputField.name.value,
           inStock : inputField.inStock.value,
           price : inputField.price.value,
           description : inputField.description.value,
           status : inputField.status.value
    }
    console.log(data);
        // console.log(JSON.stringify(data));
        
        fetch("http://127.0.0.1:8000/api/items", {
            method: 'POST',
            headers: new Headers({
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }),
            body: JSON.stringify(data),
        })
        .then(res => {
            
            console.log("Added item: " + res);
             window.location.reload();
        })
        .catch(err => {
            console.log("There was an erro posting to the api: " + err)
        }) 

    }

    
    submit(data) {
        console.log(data);
    }

    render(){
        if (!this.props.show){
            return null;
        }
        return(
            <div style={backDropStyle}>
                <div style={modalStyle}>
                    {this.props.children}
                     <div style= {footerStyle}>
                     <form style={textField} onSubmit= {this.handleSubmit}>
                        <input type="text" value={this.state.value.name} onChange= {this.handleChange} name="name" placeholder="name">{this.name}</input>
                        {/* <input type="file"  onChange= {this.handleChange} name="image" placeholder="image">{this.image}</input> */}
                        <input type="text"  value={this.state.value.inStock} onChange= {this.handleChange} name="inStock"placeholder="inStock">{this.inStock}</input>
                        <input type="text" value={this.state.value.price} onChange= {this.handleChange} name="price" placeholder="price">{this.price}</input>
                        <input type="text"  value={this.state.value.description} onChange= {this.handleChange} name="description" placeholder="description">{this.description}</input>
                        <input type="text" value={this.state.value.status} onChange= {this.handleChange} name="status" placeholder="status">{this.status}</input>
                        <button type="submit" >addTodo</button>
                </form>
                    <button onClick={(e)=> {this.onClose(e)}}>
                        Close
                    </button>
                </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired
}